<?php namespace PlanetaDelEste\Links\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateLinksTable extends Migration
{

    public function up()
    {
        Schema::create('planetadeleste_links_links', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('url');
            $table->integer('category_id')->nullable()->unsigned();
            $table->boolean('is_active')->default('1');
            $table->boolean('open_new_window')->default('1');
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetadeleste_links_links');
    }

}
