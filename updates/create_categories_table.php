<?php namespace PlanetaDelEste\Links\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('planetadeleste_links_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->string('name', 100);
            $table->string('slug', 100)->index();
            $table->boolean('is_published')->default('1');
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetadeleste_links_categories');
    }

}
