<?php namespace PlanetaDelEste\Links\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use PlanetaDelEste\Links\Models\Link;

/**
 * Links Back-end Controller
 */
class Links extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['planetadeleste.links.access_links'];
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Links', 'links', 'links');

        $this->addCss('/plugins/planetadeleste/links/assets/css/planetadeleste-links-backend.css');
    }

    /**
     * Deleted checked links.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $linkId) {
                if (!$link = Link::find($linkId)) continue;
                $link->delete();
            }

            Flash::success(Lang::get('planetadeleste.links::lang.links.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('planetadeleste.links::lang.links.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
