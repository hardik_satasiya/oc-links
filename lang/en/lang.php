<?php

return [
    'plugin'     => [
        'name'        => 'Links',
        'description' => 'No description provided yet...',
    ],
    'categories' => [
        'menu_label'              => 'Categories',
        'return_to_list'          => 'Return to Categories',
        'delete_selected_confirm' => 'Delete the selected Categories?',
        'delete_confirm'          => 'Do you really want to delete this Category?',
        'delete_selected_success' => 'Successfully deleted the selected Categories.',
        'delete_selected_empty'   => 'There are no selected Categories to delete.',
    ],
    'category'   => [
        'list_title'    => 'Manage Categories',
        'new'           => 'New Category',
        'label'         => 'Category',
        'create_title'  => 'Create Category',
        'update_title'  => 'Edit Category',
        'preview_title' => 'Preview Category',
    ],
    'links'      => [
        'menu_label'              => 'Links',
        'return_to_list'          => 'Return to Links',
        'delete_selected_confirm' => 'Delete the selected Links?',
        'delete_confirm'          => 'Do you really want to delete this Link?',
        'delete_selected_success' => 'Successfully deleted the selected Links.',
        'delete_selected_empty'   => 'There are no selected Links to delete.',
    ],
    'link'       => [
        'list_title'    => 'Manage Links',
        'new'           => 'New Link',
        'label'         => 'Link',
        'create_title'  => 'Create Link',
        'update_title'  => 'Edit Link',
        'preview_title' => 'Preview Link',
    ],
    'components' => [
        'links' => [
            'name'                 => 'Links Component',
            'description'          => 'Show a list of links by category',
            'category'             => 'Category',
            'category_description' => 'Select the category filter for links',
            'category_placeholder' => 'Choose Category',
        ],
    ],
    'form'       => [
        'fields' => [
            'title'           => 'Title',
            'url'             => 'Url',
            'category'        => 'Category',
            'overview'        => 'Overview',
            'active'          => 'Active',
            'open_new_window' => 'New window',
            'name'            => 'Name',
            'slug'            => 'Slug',
            'published'       => 'Published',
            'created'         => 'Created date',
            'updated'         => 'Updated date',
        ],
    ],
    'permission' => [
        'access_links'      => 'Access to Links',
        'access_categories' => 'Access to Categories',
    ],
];